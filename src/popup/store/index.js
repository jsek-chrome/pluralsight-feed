import Vue from "vue";
import Vuex from "vuex";
import dayjs from "dayjs";
import { persist, restore } from "./persistance";

Vue.use(Vuex);

const DATE_FORMAT = `YYYY-MM-DDTHH:MM:ss`;

const fixDates = (courses) =>
  courses.forEach(c =>
    c.displayDate = dayjs(c.displayDate).format(DATE_FORMAT)
  );

const store = new Vuex.Store({
  state: {
    courses: [],
    timeRangeLimit: dayjs().add(-2, 'week').format(DATE_FORMAT),
    prevCheck: {
      timestamp: undefined,
      ids: []
    }
  },
  getters: {
    courses(state) {
      return state.courses
        .filter(x => x.displayDate > state.timeRangeLimit);
    }
  },
  mutations: {
    courses (state, courses) {
      state.courses = courses
    }
  },
  actions: {
    saveCourses({commit}, courses) {
      fixDates(courses);
      commit('courses', courses);
      persist('local', 'courses', courses);
      setTimeout(() => {
        persist('sync', 'prevCheck', {
          timestamp: dayjs().format(`YYYY-MM-DDTHH:MM:ss`),
          ids: courses.map(c => c.prodId)
        })
      }, 300);
    }
  }
});

restore('local', 'courses').then(v => store.state.courses = v).catch(console.warn)
restore('sync', 'prevCheck').then(v => store.state.prevCheck = v).catch(console.warn)

export default store;