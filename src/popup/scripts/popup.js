import Vue from "vue";

import store from "../store"
import Popup from "../components/Popup.vue";

Vue.config.devtools = true;
new Vue({
  el: '#popup',
  store,
  render: r => r(Popup)
});