<p align="center">
  <a href="https://gitlab.com/jsek-chrome/pluralsight-feed">
    <img height="128" width="128" src="src/images/rss-4-256.png">
  </a>
  <h1 align="center">Pluralsight Feed - Browser Extension</h1>
</p>

Install from [Chrome Store](https://chrome.google.com/webstore/detail/plghffggffijpehablfafphdpopnmkgj)

---

![](screenshot.png)

## Setup

```bash
npm install
```

### Build

```bash
npm run build
```

### Pack before publish

1. Generate `key.pem` once using `npm run keygen` command

Create `*.crx` and `*.zip` packages

```bash
gulp pack
```